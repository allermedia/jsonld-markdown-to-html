# jsonld-markdown-to-html

Transforms jsonld with markdown content to html.

## installation
`npm install https://bitbucket.org/allermedia/jsonld-markdown-to-html.git --save`

## Usage 
```
const toHtml = require('jsonld-markdown-to-html');

let content = }
  'sc:body': [
    {
      '@type': 'sc:paragraph',
      text: 'This is my paragraph'
    }
  ]
};
let result = toHtml(content);
```

## Supported types
Currently supports jsonld of types:  
sc:paragraph   
sc:bodyHeading  
sc:html  
ItemList  
ImageObject  

