const marky = require('marky-markdown');

const types = require('./types');

/**
 * Iterates through the json ld and markdown, transfroms to html.
 * @param {array}   jsonld Jsonld content.
 * @return {string}
 */
const iterateContent = function(jsonld) {
  let content = '';
  if (jsonld instanceof Array) {
    jsonld.forEach(function(element) {
      if (typeof element === 'object') {
        if (element['@type'] in types) {
          content += types[element['@type']](element, iterateContent);
        } else {
          console.log('Type is not present.');
        }
      } else {
        let html = marky(element);
        // Need to strip that forced paragraph.
        content += html.substring(3,html.length - 5);
      }
    });
  } else {
    console.log('Faulty datastructure, should be array.');
  }
  return content;
};

module.exports = iterateContent;
