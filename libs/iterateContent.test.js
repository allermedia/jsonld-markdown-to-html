const assert = require('assert');

const iterateContent = require('./iterateContent.js');

describe('Function iterateContent', () => {
  it('should return expected result', function(done) {
    let testdata = [
      {
        '@type': 'sc:bodyHeading',
        text: 'My wonderful header',
        level: '1'
      },
      {
        '@type': 'sc:Paragraph',
        text: 'This is _my_ wonderful paragraph.'
      },
      {
        '@type': 'ImageObject',
        contentUrl: 'http://images.com/testimage.jpg',
      },
      {
        '@type': 'ItemList',
        name: 'The bestest things about my wonderful article',
        itemListElement: [
          'It`s _simply_ woderful',
          {
            '@type': 'ImageObject',
            contentUrl: 'http://images.com/testimage.jpg'
          }
        ]
      }
    ];
    let expectedResult = '<h1>My wonderful header</h1><p>This is <em>my</em> wonderful paragraph.</p>\n<img src="http://images.com/testimage.jpg" /><ul><li>It`s <em>simply</em> woderful</li><li><img src="http://images.com/testimage.jpg" /></li></ul>'; // jshint ignore:line
    assert.equal(iterateContent(testdata), expectedResult);
    done();
  });

  it('should return empty when no data is delivered to function', function(done) {
    let testdata = null;
    let expectedResult = ''; // jshint ignore:line
    assert.equal(iterateContent(testdata), expectedResult);
    done();
  });
});
