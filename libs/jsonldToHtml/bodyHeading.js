
/**
 * Return heading html.
 * @param {object}  element Element to convert.
 * @return {string}
 */
const bodyHeading = function(element) {
  // Default to h1;
  let headingElement = 'h1';
  if ('level' in element) {
    headingElement = 'h' + element.level;
  }
  if ('text' in element) {
    if (element.text !== '') {
      return `<${headingElement}>${element.text}</${headingElement}>`;
    } else {
      return '';
    }
  } else {
    return '';
  }
};

module.exports = bodyHeading;
