const assert = require('assert');

const html = require('./html');

describe('Function html', () => {
  it('should return expected result.', function(done) {
    let testdata = {
      '@type': 'sc:html',
      text: '<b>My wonderful bold</b>'
    };
    let expectedResult = '<b>My wonderful bold</b>';

    assert.equal(html(testdata), expectedResult);
    done();
  });

  it('should return empty string when no text property is present.', function(done) {
    let testdata = {
      '@type': 'sc:html'
    };
    let expectedResult = '';

    assert.equal(html(testdata), expectedResult);
    done();
  });
});
