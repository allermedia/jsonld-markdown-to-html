const assert = require('assert');

const itemList = require('./itemList');
const iterateContent = require('../iterateContent');

describe('Function itemList', () => {
  it('should return expected result', function(done) {
    let testdata = {
      '@type': 'ItemList',
      itemListElement: [
        'It`s simply woderful',
        {
          '@type': 'ImageObject',
          contentUrl: 'http://images.com/testimage.jpg'
        }
      ]
    };
    let expectedResult = '<ul><li>It`s simply woderful</li><li><img src="http://images.com/testimage.jpg" /></li></ul>';

    assert.equal(itemList(testdata, iterateContent), expectedResult);
    done();
  });

  it('should return empty when missing itemListelement elements', function(done) {
    let testdata = {
      '@type': 'ItemList',
      itemListElement: [
      ]
    };
    let expectedResult = '';

    assert.equal(itemList(testdata, iterateContent), expectedResult);
    done();
  });

  it('should return empty when missing itemListelement property.', function(done) {
    let testdata = {
      '@type': 'ItemList',
      itemListElement: [
      ]
    };
    let expectedResult = '';

    assert.equal(itemList(testdata, iterateContent), expectedResult);
    done();
  });
});
