
/**
 * Return image html.
 * @param {object}  element Element to convert.
 * @return {string}
 */
const imageObject = function(element) {
  if ('contentUrl' in element) {
    if (element.contentUrl !== '') {
      return `<img src="${element.contentUrl}" />`;
    } else {
      return '';
    }
  } else {
    return '';
  }
};

module.exports = imageObject;
