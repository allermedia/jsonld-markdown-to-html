
/**
 * Return List html.
 * @param {object} element    Element to convert.
 * @param {*} iterateContent  Function for recursion.
 */
const itemList = function(element, iterateContent) {
  let html = '';
  if ('itemListElement' in element) {
    if (element.itemListElement.length > 0) {
      html += '<ul>';
      element.itemListElement.forEach(function(item) {
        html += `<li>${iterateContent([item])}</li>`;
      });
      html += '</ul>';
      return html;
    } else {
      return '';
    }
  } else {
    return '';
  }
};

module.exports = itemList;
