
/**
 * Return html html.
 * @param {object} element
 * @return {string}
 */
const html = function(element) {
  if ('text' in element) {
    return element.text;
  } else {
    return '';
  }
};

module.exports = html;
