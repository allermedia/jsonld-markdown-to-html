const marky = require('marky-markdown');

/**
 * Return paragraph html.
 * @param {object} element  Element to convert.
 * @return {string}
 */
const paragraph = function(element) {
  if ('text' in element) {
    return `${marky(element.text)}`;
  } else {
    return '';
  }
};

module.exports = paragraph;
