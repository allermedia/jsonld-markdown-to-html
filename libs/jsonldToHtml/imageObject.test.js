const assert = require('assert');

const imageObject = require('./imageObject');

describe('Function imageObject', () => {
  it('should return expected result', function(done) {
    let testdata = {
      '@type': 'ImageObject',
      contentUrl: 'http://images.com/testimage.jpg'
    };
    let expectedResult = '<img src="http://images.com/testimage.jpg" />';

    assert.equal(imageObject(testdata), expectedResult);
    done();
  });

  it('should return empty when missing contentUrl property', function(done) {
    let testdata = {
      '@type': 'ImageObject'
    };
    let expectedResult = '';

    assert.equal(imageObject(testdata), expectedResult);
    done();
  });
});
