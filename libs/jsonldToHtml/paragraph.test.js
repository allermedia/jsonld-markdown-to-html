const assert = require('assert');

const paragraph = require('./paragraph');

describe('Function paragraph', () => {
  it('should return expected result', function(done) {
    let testdata = {
      '@type': 'sc:Paragraph',
      text: 'This is *the* **paragraph**'
    };
    let expectedResult = '<p>This is <em>the</em> <strong>paragraph</strong></p>\n';

    assert.equal(paragraph(testdata), expectedResult);
    done();
  });

  it('should return empty when no text is present', function(done) {
    let testdata = {
      '@type': 'sc:Paragraph',
      text: ''
    };
    let expectedResult = '';

    assert.equal(paragraph(testdata), expectedResult);
    done();
  });
});
