module.exports = {
  bodyHeading: require('./bodyHeading'),
  html: require('./html'),
  imageObject: require('./imageObject'),
  itemList: require('./itemList'),
  paragraph: require('./paragraph')
};
