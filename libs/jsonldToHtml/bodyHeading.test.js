const assert = require('assert');
const bodyHeading = require('./bodyHeading');

describe('Function bodyHeading', () => {
  it('should return expected result.', function(done) {
    let testdata = {
      '@type': 'sc:bodyHeading',
      text: 'My wonderful header',
      level: '1'
    };
    let expectedResult = '<h1>My wonderful header</h1>';

    assert.equal(bodyHeading(testdata), expectedResult);
    done();
  });

  it('should return take level into account when transforming.', function(done) {
    let testdata = {
      '@type': 'sc:bodyHeading',
      text: 'My wonderful header',
      level: '2'
    };
    let expectedResult = '<h2>My wonderful header</h2>';

    assert.equal(bodyHeading(testdata), expectedResult);
    done();
  });

  it('should return empty string when no text is supplied..', function(done) {
    let testdata = {
      '@type': 'sc:bodyHeading',
      text: '',
      level: '2'
    };
    let expectedResult = '';

    assert.equal(bodyHeading(testdata), expectedResult);
    done();
  });

  it('should default to level 1 if level is missing.', function(done) {
    let testdata = {
      '@type': 'sc:bodyHeading',
      text: 'My wonderful header'
    };
    let expectedResult = '<h1>My wonderful header</h1>';

    assert.equal(bodyHeading(testdata), expectedResult);
    done();
  });
});
