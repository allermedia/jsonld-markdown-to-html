const elements = require('./jsonldToHtml');

module.exports =
{
  'sc:bodyHeading': elements.bodyHeading,
  'sc:html': elements.html,
  'ImageObject': elements.imageObject,
  'ItemList': elements.itemList,
  'sc:Paragraph': elements.paragraph,
};
