const iterateContent = require ('./libs/iterateContent');

const jsonldMarkdownTohtml = function(jsonld) {
  if ('sc:body' in jsonld) {
    return iterateContent(jsonld['sc:body']);
  }
};

module.exports = jsonldMarkdownTohtml;
